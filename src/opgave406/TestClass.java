package opgave406;

public class TestClass {
	public String blyat;
	public int noOfCyka;

	public TestClass(String blyat, int noOfCyka) {
		super();
		this.blyat = blyat;
		this.noOfCyka = noOfCyka;
	}

	public String getBlyat() {
		return blyat;
	}

	public void setBlyat(String blyat) {
		this.blyat = blyat;
	}

	public int getNoOfCyka() {
		return noOfCyka;
	}

	public void setNoOfCyka(int noOfCyka) {
		this.noOfCyka = noOfCyka;
	}
}
